package io.gitlab.afenner99.Hunter;

import org.bukkit.GameMode;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.player.*;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import java.util.*;

// TODO: Configurable to what happens when runners die
// TODO: 1 second cooldown? (configurable) Avoid spamming the compass
// TODO: Set runner, have only one target?
public class Hunter extends JavaPlugin implements Listener
{
    public final Set<UUID> hunters = new HashSet<>();
    public final Map<World, Set<Player>> hunted = new HashMap<>(); // Each world and its respective targets.

    @Override
    public void onEnable()
    {
        this.getServer().getPluginManager().registerEvents(this, this);
        this.getCommand("hunter").setExecutor(new HunterCommand(this));
    }

    public void addHunted(Player player)
    {
        this.hunted.get(player.getWorld()).add(player);
    }

    public void removeHunted(Player player)
    {
        this.hunted.get(player.getWorld()).remove(player);
    }

    @EventHandler
    public void onPlayerInteract(PlayerInteractEvent event)
    {
        // The player uses a compass...
        ItemStack stack = event.getItem();
        if(stack != null && stack.getType() == Material.COMPASS)
        {
            // ... and is a hunter
            Player hunter = event.getPlayer();
            if(this.hunters.contains(hunter.getUniqueId()))
            {
                World hunterWorld = hunter.getWorld();

                // Get the closest player to the hunter,
                // ignoring other hunters
                Location hunterLocation = hunter.getLocation();
                Player closestPlayer = null;
                double minDistance = Double.MAX_VALUE;

                for(Player player : this.hunted.get(hunterWorld))
                {
                    double distance = player.getLocation().distance(hunterLocation);
                    if(distance < minDistance)
                    {
                        closestPlayer = player;
                        minDistance = distance;
                        if(minDistance < 1)
                            break;
                    }
                }

                // Point the compass to the hunter
                if(closestPlayer == null)
                {
                    hunter.sendMessage("§cNo players to track!");
                }
                else
                {
                    hunter.setCompassTarget(closestPlayer.getLocation());
                    hunter.sendMessage("§aCompass is now pointing to " + closestPlayer.getDisplayName() + ".");
                }
            }
        }
    }

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event)
    {
        Player player = event.getPlayer();
        if(!hunters.contains(player.getUniqueId()))
        {
            this.addHunted(player);
        }
    }

    @EventHandler
    public void onPlayerLeave(PlayerQuitEvent event)
    {
        Player player = event.getPlayer();
        if(!hunters.contains(player.getUniqueId()))
        {
            this.removeHunted(player);
        }
    }

    @EventHandler
    public void onPlayerChangedWorld(PlayerChangedWorldEvent event)
    {
        // Remove the player from the old world's list and move them to the new world's.
        Player player = event.getPlayer();
        if(!hunters.contains(player.getUniqueId()))
        {
            this.hunted.get(event.getFrom()).remove(player);
            this.addHunted(player);
        }
    }

    // Does PlayerChangedWorldEvent get called on death? I assume so...
    @EventHandler
    public void onPlayerRespawn(PlayerRespawnEvent event)
    {
        if(hunters.contains(event.getPlayer().getUniqueId()))
        {
            this.getServer().getScheduler().scheduleSyncDelayedTask(this, () ->
                    event.getPlayer().getInventory().addItem(new ItemStack(Material.COMPASS)), 2L);
        }
    }

    @EventHandler
    public void onPlayerDeath(PlayerDeathEvent event)
    {
        if(hunters.contains(event.getEntity().getUniqueId()))
        {
            event.getDrops().removeIf(drop -> drop.getType().equals(Material.COMPASS));
        }
        else if(event.getEntity().getGameMode().equals(GameMode.SURVIVAL))
        {
            event.getEntity().setGameMode(GameMode.SPECTATOR);
        }
    }
}
