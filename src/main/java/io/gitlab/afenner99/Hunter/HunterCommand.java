package io.gitlab.afenner99.Hunter;

import org.bukkit.Material;
import org.bukkit.Server;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import java.util.UUID;

public class HunterCommand implements CommandExecutor
{
    private final Hunter plugin;

    public HunterCommand(Hunter pluginIn)
    {
        this.plugin = pluginIn;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args)
    {
        Server server = sender.getServer();

        if(args.length == 1)
        {
            if(args[0].equals("list"))
            {
                if(plugin.hunters.size() == 0)
                {
                    sender.sendMessage("There are no hunters.");
                    return true;
                }

                StringBuilder stringBuilder = new StringBuilder("Hunters: ");
                for(UUID id : plugin.hunters)
                {
                    String name = server.getOfflinePlayer(id).getName();
                    if(name != null)
                        stringBuilder.append(name);
                }

                sender.sendMessage(stringBuilder.toString());

                return true;
            }
            else if(args[0].equals("clear"))
            {
                // Remove all hunters' compasses and add them to their respective worlds' lists.
                for(UUID hunterId : plugin.hunters)
                {
                    Player hunter = server.getPlayer(hunterId);
                    if(hunter != null)
                    {
                        hunter.getInventory().remove(Material.COMPASS);
                        plugin.addHunted(hunter);
                    }
                }
                // ... and clear the hunter list.
                plugin.hunters.clear();

                server.broadcastMessage("§cRemoved all hunters.");

                return true;
            }
        }
        else if(args.length == 2)
        {
            if (args[0].equals("add"))
            {
                Player hunter = server.getPlayer(args[1]);
                if (hunter == null)
                {
                    sender.sendMessage("§4Player " + args[1] + " not found.");
                    return true;
                }

                this.addHunter(hunter);

                server.broadcastMessage("§cAdded " + hunter.getDisplayName() + " as a hunter.");

                return true;
            } else if (args[0].equals("remove"))
            {
                Player hunter = server.getPlayer(args[1]);
                if (hunter == null)
                {
                    sender.sendMessage("§4Player " + args[1] + " not found.");
                    return true;
                }

                this.removeHunter(hunter);
                server.broadcastMessage("§cRemoved " + hunter.getDisplayName() + " as a hunter.");

                return true;
            }
        }

        return false;
    }

    private void addHunter(Player hunter)
    {
        UUID hunterId = hunter.getUniqueId();
        if(plugin.hunters.contains(hunterId))
            return;

        plugin.hunters.add(hunter.getUniqueId());
        // Give the hunter a compass:
        hunter.getInventory().addItem(new ItemStack(Material.COMPASS));
        plugin.removeHunted(hunter);
    }

    private void removeHunter(Player hunter)
    {
        UUID hunterId = hunter.getUniqueId();
        if(!plugin.hunters.contains(hunterId))
            return;

        plugin.hunters.remove(hunterId);
        // Remove any compasses from their inventory:
        hunter.getInventory().remove(Material.COMPASS);
        plugin.addHunted(hunter);
    }
}
